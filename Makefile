ENV = env
BIN = $(ENV)/bin
PIP = $(BIN)/pip
PYTHON = $(BIN)/python
PIP_COMPILE = $(BIN)/pip-compile
PYTEST = $(BIN)/pytest


environ: $(ENV)/.done

.PHONY: help
help:
	@echo "make                         # build everything"
	@echo "make requirements            # build requirements*.txt from requirements*.in"
	@echo "make upgrade-requirements    # upgrade requirements*.txt from requirements*.in"
	@echo "make run                     # run dev server"
	@echo "make test                    # run unit tests"
	@echo "make tags                    # build ctags file"

$(PIP):
	python -m venv env
	$(PIP) install -U pip setuptools wheel pip-tools pre-commit

$(ENV)/.done: $(PIP) requirements-dev.txt
	$(PIP) install -r requirements-dev.txt -e .
	touch env/.done

.PHONY: run
run: environ
	$(PYTHON) manage.py runserver

.PHONY: test
test: environ
	$(PYTEST) -vv --tb=native --cov=my_restaurant_vote --cov=api --cov-fail-under=75 --cov-report=term-missing tests

.PHONY: requirements
requirements: $(PIP) requirements.in requirements-dev.in
	$(PIP_COMPILE) requirements.in -o requirements.txt
	$(PIP_COMPILE) requirements.in requirements-dev.in -o requirements-dev.txt

.PHONY: upgrade-requirements
upgrade-requirements: $(PIP) requirements.in requirements-dev.in
	$(PIP_COMPILE) -U requirements.in -o requirements.txt
	$(PIP_COMPILE) -U requirements.in requirements-dev.in -o requirements-dev.txt

.PHONY: tags
tags:
	ctags -R env api my_restaurant_vote tests

.PHONY: clean
clean: clean_pycache
	rm -rf $(ENV) my_restaurant_vote.egg-info .pytest_cache

.PHONY: clean_pycache
.clean_pycache:
	find . -name "__pycache__" -type d -exec rm -rf "{}" +
	find . -name "*.pyc" -delete
