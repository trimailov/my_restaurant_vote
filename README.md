# My Restaurant Vote

A REST API demo application for restaurant voting

## Table of contents

- [Demo](#demo)
- [Goals](#goals)
- [Features](#features)
- [How to use API](#how-to-use-api)
- [How to develop](#how-to-develop):
    - [Local development with Docker](#local-dev-in-docker)
    - [Local development without Docker](#local-dev):
        - [Issues with psycopg2](#issue-w-psycopg2)
    - [Requirements](#requirements)
    - [Pre-commit hooks](#pre-commit-hooks)
- [Best practices](#best-practices)
- [API examples](#api-examples)

## <a name="demo"></a>Demo

Demo application is deployed and available at heroku: [https://my-restaurant-vote.herokuapp.com](https://my-restaurant-vote.herokuapp.com/)

This demo app is available with HTTPS, thus your passwords should be safe(r) from vilanous eyes.

Visitors are welcome to try it out. How to use this API is listed [below](#how-to-use-api).

App may be slow to respond for the first request, due heroku sending the app to sleep state.

## <a name="goals"></a>Goals

Goals for this API:

1. Everyone can add/remove/update restaurants
2. Every user gets X (hardcoded, but "configurable") votes per day. First user vote on the same restaurant counts as 1, second as 0.5, 3rd and all subsequent votes, as 0.25.
    - If a voting result is the same in a couple of restaurants, the winner is the one who got more distinct users to vote on it.
3. Every day vote amounts are reset. Not used previous day votes are lost.
4. Show the history of selected restaurants per time period
5. Do not forget, need a way to show on what restaurants users can vote and what restaurant is a winner.

## <a name="features"></a>Features

- Authentication:
    - [X] Username and passowrd to authenticate and receive an API token;
    - [X] Most endpoints require token for authorization;
    - [X] Token invalidation endpoint (logout);
- Users:
    - [X] Create a user;
- Restaurants:
    - [X] Create/Retrieve/Update/Delete restaurants;
- Voting:
    - [X] Vote for restaurant;
    - [X] Limit votes per day;
    - [X] Calculate restaurant votes;
    - [X] Calculate winner restaurant;
    - [X] List winner restaurants for time period;
- Pagination:
    - [X] List views return 10 results;
    - [X] JSON response have `next` and `prev` keys with links to next and previous pages;

## <a name="how-to-use-api"></a>How to use the API

Here is listed all available REST API endpoints. Each endpoint has example usage linked to examples below.

All API endpoints (except login and create user) require authorization token in request headers, e.g.: `Authorization: Token <my_token>`.

Typical use:
- Create your user: `POST /auth/users/`;
- Login: `POST /auth/token/login`;
- Create restaurants if necessary: `POST /api/v1/restaurants/`;
- Vote: `POST /api/v1/restaurants/<id>/vote/`;
- Get winner: `GET /api/v1/restaurants/todays_winner/`;
- Logout: `POST /auth/token/logout`;
- Enjoy your lunch;

| Method  | Endpoint                                                                                       | Description                                         | Status |
| ------  | ---------------------------------------------------------------------------------------------- | --------------------------------------------------- | ------ |
| POST    | [`/auth/users/`](#user-login)                                                                  | Create user                                         | 201    |
| Accpets | username                                                                                       | username [string] (required)                        |        |
|         | password                                                                                       | password [string] (required)                        |        |
|         |                                                                                                |                                                     |        |
| GET     | [`/auth/users/me/`](#user-details)                                                             | Get user details                                    | 200    |
| Returns | username                                                                                       | username [string]                                   |        |
|         | email                                                                                          | email [string]                                      |        |
|         | id                                                                                             | id [int]                                            |        |
|         |                                                                                                |                                                     |        |
| POST    | [`/auth/token/login/`](#login)                                                                 | Login user                                          | 201    |
| Accpets | username                                                                                       | username [string] (required)                        |        |
|         | password                                                                                       | password [string] (required)                        |        |
|         |                                                                                                |                                                     |        |
| POST    | [`/auth/token/logout/`](#logout)                                                               | Logout user                                         | 204    |
| Accpets | token from "Authorization" header                                                              |                                                     |        |
|         |                                                                                                |                                                     |        |
| POST    | [`/api/v1/restaurants/`](#restaurant-create)                                                   | Create restaurant                                   | 201    |
| Accpets | name                                                                                           | restaurant name [string] (required, unique)         |        |
|         | menu                                                                                           | menu [json] (required)                              |        |
|         |                                                                                                |                                                     |        |
| GET     | [`/api/v1/restaurants/`](#restaurant-list)                                                     | Get all restaurants (paginated)                     | 200    |
| Returns | paginated list of restaurant objects with:                                                     |                                                     |        |
|         | id                                                                                             | id [int]                                            |        |
|         | name                                                                                           | restaurant name [string]                            |        |
|         | menu                                                                                           | menu [json]                                         |        |
|         |                                                                                                |                                                     |        |
| GET     | [`/api/v1/restaurants/<id>/`](#restaurant-get)                                                 | Get single restaurant                               | 200    |
| Returns | id                                                                                             | id [int]                                            |        |
|         | name                                                                                           | restaurant name [string]                            |        |
|         | menu                                                                                           | menu [json]                                         |        |
|         |                                                                                                |                                                     |        |
| PATCH   | [`/api/v1/restaurants/<id>/`](#restaurant-patch)                                               | Edit restaurant                                     | 200    |
| Accpets | name                                                                                           | restaurant name [string]                            |        |
|         | menu                                                                                           | menu [json]                                         |        |
|         |                                                                                                |                                                     |        |
| PUT     | [`/api/v1/restaurants/<id>/`](#restaurant-put)                                                 | Edit restaurant                                     | 200    |
| Accpets | name                                                                                           | restaurant name [string] (required, unique)         |        |
|         | menu                                                                                           | menu [json] (required)                              |        |
|         |                                                                                                |                                                     |        |
| DELETE  | [`/api/v1/restaurants/<id>/`](#restaurant-delete)                                              | Delete restaurant                                   | 204    |
|         |                                                                                                |                                                     |        |
| POST    | [`/api/v1/restaurants/<id>/vote/`](#restaurant-vote)                                           | Vote for the restaurant                             | 200    |
|         |                                                                                                |                                                     |        |
| GET     | [`/api/v1/restaurants/todays_winner/`](#restaurant-todays-winner)                              | Get winner restaurant for today                     | 200    |
| Returns | id                                                                                             | id [int]                                            |        |
|         | name                                                                                           | restaurant name [string]                            |        |
|         | date                                                                                           | date of voting [string]                             |        |
|         | vote_count                                                                                     | total votes received for restaurant [float]         |        |
|         |                                                                                                |                                                     |        |
| GET     | [`/api/v1/restaurants/winners/`](#restaurant-all-winners)                                      | Get all winner restaurants (paginated)              | 200    |
| Returns | paginated list of restaurant voting winner objects with:                                       |                                                     |        |
|         | id                                                                                             | id [int]                                            |        |
|         | name                                                                                           | restaurant name [string]                            |        |
|         | date                                                                                           | date of voting [string]                             |        |
|         | vote_count                                                                                     | total votes received for restaurant [float]         |        |
|         |                                                                                                |                                                     |        |
| GET     | [`/api/v1/restaurants/winners/?date__gt=<date>&date__lt=<date>`](#restaurant-filtered-winners) | Get winner restaurants for time period (paginated)] | 200    |
| Returns | paginated list of restaurant voting winner objects with:                                       |                                                     |        |
|         | id                                                                                             | id [int]                                            |        |
|         | name                                                                                           | restaurant name [string]                            |        |
|         | date                                                                                           | date of voting [string]                             |        |
|         | vote_count                                                                                     | total votes received for restaurant [float]         |        |

## <a name="how-to-develop"></a>How to develop

### Tech stack:

On your machine you should have:
- [Docker](https://docs.docker.com/get-started/#download-and-install-docker-desktop);
- [docker-compose](https://docs.docker.com/compose/install/);
- Python 3.8.6 (If you want to develop on local machine, otherwise `Docker` should be enough);

This API is built using:
- [Django](https://www.djangoproject.com/);
- [Django REST Framework](https://www.django-rest-framework.org/);
- [Postgresql](https://www.postgresql.org/);

### <a name="local-dev-in-docker"></a>Local development in Docker

There's `Dockerfile.dev` and `docker-compose.dev.yml` for development purposes.

To build, run and develop app using `docker run`:

```
docker-compose -f docker-compose.dev.yml up --build
```

To run commands on the app container:

```
docker-compose -f docker-compose.dev.yml exec api <command> <arg1> <option2>
```

Application should be reachable at [http://localhost:8000/](http://localhost:8000/)

### <a name="local-dev"></a>Local development without Docker

I usually prefer to run the developed code on local machine and launch only other services with docker.

To build environment for web server development on local machine:

```
# creates python virtual environment and installs requirements
make
```

To run:

```
# make sure postgres service is running
docker-compose -f docker-compose.dev.yml up postgres

# builds environment (if necessary) and runs the web server
make run
```

Application should be reachable at [http://localhost:8000/](http://localhost:8000/)

#### <a name="issue-w-psycopg2"></a>Issues with psycopg2 and local build

`psycopg2` may not install on your local machine because it cannot find `openssl`.

You need to set `LDFLAGS` and `CPPFLAGS` env variable to openssl `lib` and `include` dirs.
In my case (using brew on macos) it was `"-L/usr/local/opt/openssl/lib"` and `"-L/usr/local/opt/openssl/include"` respectively.

### <a name="requirements"></a>Requirements

Edit `requirements.in` or `requirements-dev.in` to change required packages.

To build `requirements*.txt` files:

```
make requirements
```

To upgrade package versions in `requirements*.txt` files:

```
make upgrade-requirements
```

### <a name="pre-commit-hooks"></a>Pre-commit hooks

To avoid code quality issues, contributors are encouraged to use [pre-commit hooks](https://pre-commit.com/#intro).
Otherwise linters and formatters may not allow pipelines to pass.

There are multiple ways to install `pre-commit` hooks:

- If you've build local dev environment using `make`:
```
env/bin/pre-commit install
env/bin/pre-commit install-hooks
```

- If you have not run `make` yet and develop in `docker` only, I still encourage to install `pip-tools` in virtual environment, to do so run:
```
# create virtual environment for python
make env/bin/pip

# and then repeat same pre-commit install commands:
env/bin/pre-commit install
env/bin/pre-commit install-hooks
```

- Otherwise install `pre-commit` using any way convenient for you (e.g. global `pip`) and run same commands as above:
```
pre-commit install
pre-commit install-hooks
```

## <a name="best-pracitces"></a>Best practices

This demo project tries to use various best practices:
- [X] Version control in git;
- [X] Continuous integration to lint, check code formatting and automate tests;
- [X] Continuous deployment to build Docker image and deploy to Heroku;
- [X] Local development in python virtual environment with `make`;
- [X] Alternatively - local development in Docker container with `docker-compose`;
- [X] Dependent services run in Docker (e.g. Postgres);
- [X] Automated tests with code coverage checking and tracking in CI analytics;
- [X] Deployment secrets are stored in CI/CD environment and are not exposed in version control or any other non-authorized way;
- [X] Middleware to log some details on requests (response time, user id, status, endpoint, query, etc.) to console;
- [X] Using Gitlab issue tracking for feature implementations;
- [X] Using merge requests to develop features and fix issues without poluting master branch with possibly broken state;

Known issues and shortcomings:
- This demo application assumes it's use by one company only;
    - Either `company` model is necessary or this application should be accessible through private VPN;
- Would be nice to have better deployment with automated migrations:
    - In heroku's container environment it's not so easy to setup deployment stages as with Procfile
- Logging:
    - Could use something like graylog service, but it's deployment and management is out of scope of a small demo;
    - Heroku has efemeral filesystem and limited console length so logs are not persistant;
- Tests should run on final wheel or docker image:
    - Though this setup is little bit more complex and not implemented yet, due to time constraints;
- Would be nice to automate API documentation with swagger, but not done yet, due to time constraints;

## <a name="api-examples"></a>API use examples:

<a name="create-user"></a>Create user:
```bash
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  https://my-restaurant-vote.herokuapp.com/auth/users/ \
  --data '{"username": "foo", "password": "letmetest"}'
{
    "email": "",
    "username": "foo",
    "id": 2
}
```

<a name="user-details"></a>User details:
```bash
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/auth/users/me/
{
    "email": "",
    "id": 2,
    "username": "foo"
}
```

<a name="login"></a>Login:
```bash
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  https://my-restaurant-vote.herokuapp.com/auth/token/login/ \
  --data '{"username": "foo", "password": "letmetest"}'
{
    "auth_token": "f6c0d66d6116e42adcd867f3adcec650b2ff7e1d"
}
```

<a name="logout"></a>Logout:
```bash
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  "https://my-restaurant-vote.herokuapp.com/auth/token/logout/"

# attempt to use same token:
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  "https://my-restaurant-vote.herokuapp.com/auth/users/me/"
{
    "detail": "Invalid token."
}
```

<a name="restaurant-create"></a>Create a restaurant:
```bash
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/ \
  --data '{"name": "cool restaurant", "menu": {"salad": 4.50, "main": 8.0}}'
{
    "id": 1,
    "name": "cool restaurant",
    "menu": {
        "salad": 4.5,
        "main": 8.0
    }
}
```

<a name="restaurant-list"></a>List all restaurants:
```bash
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "name": "cool restaurant",
            "menu": {
                "main": 8.0,
                "salad": 4.5
            }
        },
        {
            "id": 2,
            "name": "hip restaurant",
            "menu": {
                "kambucha": 4.2,
                "budha bowl": 7.5
            }
        },
        {
            "id": 3,
            "name": "fast food",
            "menu": {
                "kebab": 3.99,
                "burger": 4.99
            }
        }
    ]
}
```

<a name="restaurant-get"></a>Get a restaurant:
```bash
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/1/
{
    "id": 1,
    "name": "cool restaurant",
    "menu": {
        "main": 8.0,
        "salad": 4.5
    }
}
```

<a name="restaurant-patch"></a>PATCH a restaurant:
```bash
curl -X PATCH \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/1/ \
  --data '{"name": "super cool restaurant"}'
{
    "id": 1,
    "name": "super cool restaurant",
    "menu": {
        "main": 8.0,
        "salad": 4.5
    }
}
```

<a name="restaurant-put"></a>PUT a restaurant:
```bash
curl -X PUT \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/1/ \
  --data '{"name": "super cool restaurant", "menu": {"steak": 40.0}}'
{
    "id": 1,
    "name": "super cool restaurant",
    "menu": {
        "steak": 40.0
    }
}
```

<a name="restaurant-delete"></a>Delete a restaurant:
```bash
curl -X DELETE \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/1/
```

<a name="restaurant-vote"></a>Vote for a restaurant:
```bash
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/2/vote/
{
    "restaurant": 2,
    "user": 2,
    "vote_value": 1.0
}

# voting a second time in a same day for the restaurant will lower vote value
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token f6c0d66d6116e42adcd867f3adcec650b2ff7e1d" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/2/vote/
{
    "restaurant": 2,
    "user": 2,
    "vote_value": 0.5
}
```

<a name="restaurant-todays-winner"></a>Get todays winner restaurant:
```bash
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/todays_winner/
{
    "date": "2020-10-26",
    "id": 2,
    "name": "hip restaurant",
    "vote_count": 2.5
}

# in case there are no votes today, 404 is returned:
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/todays_winner/
{
    "not_found": "no votes today"
}
```

<a name="restaurant-all-winners"></a>Get all winner restaurants:
```bash
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/winners/
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "date": "2020-10-26",
            "id": 2,
            "name": "hip restaurant",
            "vote_count": 2.5
        }
    ]
}
```

<a name="restaurant-filtered-winners"></a>Get winner restaurants filtered by time period:
```bash
# these filters are supported in GET query:
# - date__lte=<yyyy-mm-dd>
# - date__lt=<yyyy-mm-dd>
# - date__gte=<yyyy-mm-dd>
# - date__gt=<yyyy-mm-dd>
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json; indent=4" \
  -H "Authorization: Token 06f599ee0265ffb597f535e5e381e9c7d98096b9" \
  "https://my-restaurant-vote.herokuapp.com/api/v1/restaurants/winners/?date__lte=2020-10-26"
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "date": "2020-10-26",
            "id": 2,
            "name": "hip restaurant",
            "vote_count": 2.5
        }
    ]
```
