from setuptools import setup


def read_reqs(filename):
    with open(filename) as f:
        return [req for req in (req.partition("#")[0].strip() for req in f) if req]


setup(
    name="my_restaurant_vote",
    version="1.0",
    description="API for restaurant vote",
    author="Justas Trimailovas",
    author_email="j.trimailovas@gmail.com",
    url="https://gitlab.com/trimailov/my_restaurant_vote",
    install_requires=read_reqs("requirements.in"),
)
