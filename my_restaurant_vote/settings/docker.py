from my_restaurant_vote.settings.base import *  # noqa

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "my_restaurant_vote",
        "USER": "admin",
        "PASSWORD": "secret",
        "HOST": "postgres",
        "PORT": "5432",
    }
}
