import factory
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from api.models import Restaurant, RestaurantUserVote


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: "user%d" % n)
    email = factory.Sequence(lambda n: "user%d@example.com" % n)
    password = factory.PostGenerationMethodCall("set_password", "letmetest")


class TokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Token

    user = factory.SubFactory(UserFactory)


class RestaurantFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Restaurant

    name = factory.Sequence(lambda n: f"Restaurant #{n}")
    menu = [["pizza", 7.5], ["kebab", 3.0]]


class RestaurantUserVoteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RestaurantUserVote

    # `vote_value` is created automatically through overriden RestaurantUserVote.save()
    # `created` is set with `auto_now_add` flage set to True
    user = factory.SubFactory(UserFactory)
    restaurant = factory.SubFactory(RestaurantFactory)
