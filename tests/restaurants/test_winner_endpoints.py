import pytest
from django.urls import reverse

from tests.factories import (
    RestaurantFactory,
    RestaurantUserVoteFactory,
    UserFactory,
)

pytestmark = pytest.mark.django_db


def setup_multi_day_voting(freezer):
    # Create test data to test voting winners
    # It would be excellent to think of way to call this only once in this module
    u1 = UserFactory()
    u2 = UserFactory()
    u3 = UserFactory()
    r1 = RestaurantFactory()
    r2 = RestaurantFactory()
    r3 = RestaurantFactory()

    # freeze time to some day
    freezer.move_to("2020-03-11 12:00:00")
    RestaurantUserVoteFactory(user=u1, restaurant=r3)

    # bump time to other day
    freezer.move_to("2020-03-12 12:00:00")
    # scheme for this day provides that r2 wins, because it has more distinct users than r1:
    # r1.score is 3.5 and 2 users:
    #   u1: 1 + 0.5 + 0.25 + 0.25;
    #   u2: 1 + 0.5;
    # r2.score is 3.5 and 3 users:
    #   u1: 1;
    #   u2: 1 + 0.5;
    #   u3: 1;
    # r3.score is 1 and 1 user:
    #   u3: 1;

    # user1 votes 4 times for r1 and 1 time for r2
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r2)

    # user2 votes 2 times for r1 and 2 times for r2
    RestaurantUserVoteFactory(user=u2, restaurant=r1)
    RestaurantUserVoteFactory(user=u2, restaurant=r1)
    RestaurantUserVoteFactory(user=u2, restaurant=r2)
    RestaurantUserVoteFactory(user=u2, restaurant=r2)

    # user3 votes 1 time for r2 and 1 time for r3
    RestaurantUserVoteFactory(user=u3, restaurant=r2)
    RestaurantUserVoteFactory(user=u3, restaurant=r3)

    # bump time to other day (with a gap)
    freezer.move_to("2020-03-14 12:00:00")
    RestaurantUserVoteFactory(user=u1, restaurant=r2)

    # bump time to other day
    freezer.move_to("2020-03-15 12:00:00")
    # scheme for this day provides that r1 wins, because:
    # r1.score is 3.25:
    #   u1: 1 + 0.5 + 0.25;
    #   u2: 1 + 0.5;
    # r2.score is 2.5:
    #   u1: 1;
    #   u2: 1 + 0.5;

    # user1 votes 3 times for r1 and 1 time for r2
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r1)
    RestaurantUserVoteFactory(user=u1, restaurant=r2)

    # user2 votes 2 times for r1 and 2 times for r2
    RestaurantUserVoteFactory(user=u2, restaurant=r1)
    RestaurantUserVoteFactory(user=u2, restaurant=r1)
    RestaurantUserVoteFactory(user=u2, restaurant=r2)
    RestaurantUserVoteFactory(user=u2, restaurant=r2)

    # bump time to other day (with a gap)
    freezer.move_to("2020-03-16 12:00:00")
    RestaurantUserVoteFactory(user=u1, restaurant=r3)

    return r1, r2, r3


def test_todays_restaurant_winner_no_auth(client, freezer):
    r1, _, _ = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-15 12:00:00")
    resp = client.get(reverse("api:restaurant-todays-winner"))
    assert resp.status_code == 401


def test_todays_restaurant_no_votes_yet(auth_client, freezer):
    # tests getting todays winner when there are no votes yet
    resp = auth_client.get(reverse("api:restaurant-todays-winner"))
    assert resp.status_code == 404


def test_todays_restaurant_winner(auth_client, freezer):
    r1, _, _ = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-15 12:00:00")
    resp = auth_client.get(reverse("api:restaurant-todays-winner"))
    assert resp.status_code == 200
    assert resp.json()["id"] == r1.id
    assert resp.json()["vote_count"] == 3.25


def test_todays_restaurant_winner_same_vote_value(auth_client, freezer):
    _, r2, _ = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-12 12:00:00")
    resp = auth_client.get(reverse("api:restaurant-todays-winner"))
    assert resp.status_code == 200
    assert resp.json()["id"] == r2.id
    assert resp.json()["vote_count"] == 3.5


def test_restaurant_winners_no_auth(client, freezer):
    r1, r2, r3 = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-16 12:00:00")
    resp = client.get(reverse("api:restaurant-winners"))
    assert resp.status_code == 401


def test_restaurant_winners_no_votes(auth_client, freezer):
    # tests endpoint for all winners report, when there are no votes
    resp = auth_client.get(reverse("api:restaurant-winners"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 0


def test_restaurant_winners_time_filters(auth_client, freezer):
    # Tests return of winners over time period
    r1, r2, r3 = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-16 12:00:00")
    resp = auth_client.get(
        reverse("api:restaurant-winners") + "?date__gte=2020-03-12&date__lt=2020-03-16"
    )
    assert resp.status_code == 200
    assert resp.json()["count"] == 3
    assert resp.json()["results"] == [
        {
            "date": "2020-03-15",
            "id": r1.id,
            "name": r1.name,
            "vote_count": 3.25,
        },
        {
            "date": "2020-03-14",
            "id": r2.id,
            "name": r2.name,
            "vote_count": 1.0,
        },
        {
            "date": "2020-03-12",
            "id": r2.id,
            "name": r2.name,
            "vote_count": 3.5,
        },
    ]


def test_restaurant_winners_other_time_filters(auth_client, freezer):
    # Tests return of winners over time period, uses other time filters, than previous test
    r1, r2, r3 = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-16 12:00:00")
    resp = auth_client.get(
        reverse("api:restaurant-winners") + "?date__gt=2020-03-12&date__lte=2020-03-16"
    )
    assert resp.status_code == 200
    assert resp.json()["count"] == 3
    assert resp.json()["results"] == [
        {
            "date": "2020-03-16",
            "id": r3.id,
            "name": r3.name,
            "vote_count": 1.0,
        },
        {
            "date": "2020-03-15",
            "id": r1.id,
            "name": r1.name,
            "vote_count": 3.25,
        },
        {
            "date": "2020-03-14",
            "id": r2.id,
            "name": r2.name,
            "vote_count": 1.0,
        },
    ]


def test_restaurant_winners_all_times(auth_client, freezer):
    r1, r2, r3 = setup_multi_day_voting(freezer)

    freezer.move_to("2020-03-16 12:00:00")
    resp = auth_client.get(reverse("api:restaurant-winners"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 5
    assert resp.json()["results"] == [
        {
            "date": "2020-03-16",
            "id": r3.id,
            "name": r3.name,
            "vote_count": 1.0,
        },
        {
            "date": "2020-03-15",
            "id": r1.id,
            "name": r1.name,
            "vote_count": 3.25,
        },
        {
            "date": "2020-03-14",
            "id": r2.id,
            "name": r2.name,
            "vote_count": 1.0,
        },
        {
            "date": "2020-03-12",
            "id": r2.id,
            "name": r2.name,
            "vote_count": 3.5,
        },
        {
            "date": "2020-03-11",
            "id": r3.id,
            "name": r3.name,
            "vote_count": 1.0,
        },
    ]
