import pytest
from django.conf import settings
from django.urls import reverse

from api.models import RestaurantUserVote
from tests.factories import (
    RestaurantFactory,
    RestaurantUserVoteFactory,
    TokenFactory,
    UserFactory,
)

pytestmark = pytest.mark.django_db


def test_get_restaurants_no_auth(client):
    RestaurantFactory()
    RestaurantFactory()
    resp = client.get(reverse("api:restaurant-list"))
    assert resp.status_code == 401


def test_get_restaurants(auth_client):
    RestaurantFactory()
    RestaurantFactory()
    resp = auth_client.get(reverse("api:restaurant-list"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 2


def test_create_restaurant(auth_client):
    resp = auth_client.post(
        reverse("api:restaurant-list"),
        {"name": "starbucks", "menu": {"latte": 10, "espresso": 5}},
        format="json",
    )
    assert resp.status_code == 201
    assert list(resp.json().keys()) == ["id", "name", "menu"]


def test_vote_restaurant(auth_client):
    r1 = RestaurantFactory()

    # sanity check
    assert RestaurantUserVote.objects.count() == 0

    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert list(resp.json().keys()) == ["restaurant", "user", "vote_value"]
    assert RestaurantUserVote.objects.count() == 1


# freeze time to get deterministic results, e.g. tests run at midnight
@pytest.mark.freeze_time("2020-03-14 12:00:00")
def test_vote_restaurant_multiple_times(auth_client):
    r1 = RestaurantFactory()
    r2 = RestaurantFactory()

    # voting for first restaurant first time - sets restaurant vote value to 1.0
    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 1.0

    # voting for first restaurant second time - sets restaurant vote value to 0.5
    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 0.5

    # voting for first restaurant third time and later - sets restaurant vote value to 0.25
    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 0.25

    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 0.25

    # voting for second restaurant first time - sets restaurant vote value to 1.0
    resp = auth_client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r2.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 1.0


def test_vote_restaurant_multiple_times_multiple_days(client, freezer):
    # tests that voting next day for the same restaurant will start vote values from 1.0

    u1 = UserFactory()
    r1 = RestaurantFactory()

    token = TokenFactory(user=u1)
    client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

    # create max votes for a restaurant
    freezer.move_to("2020-03-14 12:00:00")
    for i in range(settings.MRV_MAX_DAILY_USER_VOTES):
        RestaurantUserVoteFactory(user=u1, restaurant=r1)

    # voting for restaurant first time other day - sets restaurant vote value back to 1.0
    freezer.move_to("2020-03-15 12:00:00")
    resp = client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 1.0

    # voting for the restaurant second time other day - sets restaurant vote value to 0.5
    resp = client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 0.5

    # voting for the restaurant third time and later - sets restaurant vote value to 0.25
    resp = client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 200
    assert resp.json()["vote_value"] == 0.25

    assert RestaurantUserVote.objects.count() == settings.MRV_MAX_DAILY_USER_VOTES + 3


def test_vote_restaurant_max_votes(client):
    u1 = UserFactory()
    r1 = RestaurantFactory()

    token = TokenFactory(user=u1)
    client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

    # create votes at max limit, note that only user is locked, new restaurants
    # are created with each vote factory creation, thus we make sure that
    # max voting is checked across all daily restaurant votes
    for i in range(settings.MRV_MAX_DAILY_USER_VOTES):
        RestaurantUserVoteFactory(user=u1)

    # vote one more time, which should be over the limit
    resp = client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "User exceeded daily voting limit of 10 votes."
    ]


def test_vote_restaurant_max_votes_multiple_days(client, freezer):
    # tests that unused votes from previous days are not moved to next day
    u1 = UserFactory()
    r1 = RestaurantFactory()

    token = TokenFactory(user=u1)
    client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

    # save one vote on some day
    freezer.move_to("2020-03-14 12:00:00")
    for i in range(settings.MRV_MAX_DAILY_USER_VOTES - 1):
        RestaurantUserVoteFactory(user=u1)

    # vote max amount of times next day
    freezer.move_to("2020-03-15 12:00:00")
    for i in range(settings.MRV_MAX_DAILY_USER_VOTES):
        RestaurantUserVoteFactory(user=u1)

    # try to vote for one more time and make sure we get error
    resp = client.post(
        reverse("api:restaurant-vote", kwargs={"pk": r1.id}),
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "User exceeded daily voting limit of 10 votes."
    ]
