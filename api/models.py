from itertools import groupby

from django.conf import settings
from django.core.serializers import json
from django.db import models
from django.utils import timezone


class Restaurant(models.Model):
    name = models.CharField(max_length=100, unique=True)
    menu = models.JSONField(encoder=json.DjangoJSONEncoder)

    def __str__(self):
        return f"{self.name}"


class RestaurantUserVoteQuerySet(models.QuerySet):
    def get_restaurant_votes(self):
        # Queries votes grouped by restaurant and creation date, then sums restaurant's votes
        # and counts distinct voters (users), then returns ordered results by date, vote sum,
        # and user count.
        # Ordering makes sure that for each day, first result is the winner restaurant.
        #
        # NB: results are ordered by created__date first, because we later use python's groupby()
        # which works a bit differently than SQL, groups results when key function changes
        # https://docs.python.org/3.8/library/itertools.html?highlight=groupby#itertools.groupby
        #
        # NB: there's `restaurant__name` in `values` call, as a convenience to return
        # restaurant names without making secondary calls to db for winner restaurants.
        #
        # XXX: I have not thought yet how to calculate this query without nested GROUP BY, i.e.
        # we group by restaurants and creation date to get the vote sums. Though to get winners
        # for a time period, we need to GROUP BY creation date one more time.
        # I have not found a way to do this in Django (yet? :) ). Dealing with raw SQL is not
        # much better either. Probably could use something like `crontab` to count and mark
        # with a flag each days winners. Then getting results over time period would be
        # a simple queryset filter. But it's just moving complexity on to infrastructure management.
        result = (
            self.values("restaurant_id", "created__date", "restaurant__name")
            .annotate(models.Sum("vote_value"), models.Count("user", distinct=True))
            .order_by("-created__date", "-vote_value__sum", "-user__count")
        )
        return result

    def get_todays_winner(self):
        # Returns a dictionary data for todays restaurant winner.
        # Would be great to have model instance returned, but `qs.values()` lose them.

        results = self.filter(created__date=timezone.localdate()).get_restaurant_votes()
        if results:
            # winner is the first value in result list
            result = results[0]
            winner_restaurant = {
                "date": result["created__date"],
                "id": result["restaurant_id"],
                "name": result["restaurant__name"],
                "vote_count": result["vote_value__sum"],
            }
            return winner_restaurant
        else:
            return None

    def get_winners(self):
        # Returns a list of restaurant winners in dictionary form.
        # Can be called after we filter queryset to our needs (e.g. time period):
        # >> RestaurantUserVote.objects.filter(created__gte=<date>).get_winners()
        result = self.get_restaurant_votes()
        winners = []

        # This runs returned vote count result values through a python's `groupby`, to group
        # results by date and get each days winner.
        for date, group in groupby(result, lambda x: x["created__date"]):
            for winner in group:
                winners.append(
                    {
                        "date": date,
                        "id": winner["restaurant_id"],
                        "name": winner["restaurant__name"],
                        "vote_count": winner["vote_value__sum"],
                    }
                )
                # First value in a date result list is a winner, so user break to move to other
                # days. This implementation seemed to me more semantic than calling `__next__()`
                # in try/except block on a `group` iterator.
                break
        return winners


class RestaurantUserVote(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    vote_value = models.FloatField()

    objects = RestaurantUserVoteQuerySet.as_manager()

    def __str__(self):
        return (
            f"r={self.restaurant}, u={self.user}, v={self.vote_value} @ {self.created}"
        )

    def save(self, *args, **kwargs):
        self.set_vote_value()
        super().save(*args, **kwargs)

    def set_vote_value(self):
        # This method sets vote_value for the RestaurantUserVote instance,
        # i.e. it counts how many times user voted for specific restaurant today
        # to get the scaled vote value.
        # This method does not validate if user is voting above allowed daily vote limit!
        restaurant_daily_votes = self.__class__.objects.filter(
            user=self.user,
            restaurant=self.restaurant,
            created__date=timezone.localdate(),
        ).count()

        if restaurant_daily_votes == 0:
            self.vote_value = 1.0
        elif restaurant_daily_votes == 1:
            self.vote_value = 0.5
        else:
            self.vote_value = 0.25
