from django_filters import rest_framework as filters

from api.models import RestaurantUserVote


class RestaurantUserVoteDateRangeFilter(filters.FilterSet):
    date__lte = filters.DateFilter(field_name="created__date", lookup_expr="lte")
    date__lt = filters.DateFilter(field_name="created__date", lookup_expr="lt")
    date__gte = filters.DateFilter(field_name="created__date", lookup_expr="gte")
    date__gt = filters.DateFilter(field_name="created__date", lookup_expr="gt")

    class Meta:
        model = RestaurantUserVote
        fields = ("created",)
