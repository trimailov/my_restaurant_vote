from django.conf import settings
from django.utils import timezone
from rest_framework import serializers

from api.models import Restaurant, RestaurantUserVote


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        read_only_fields = ("id",)
        fields = (
            "id",
            "name",
            "menu",
        )


class RestaurantWinnerSerializer(serializers.Serializer):
    date = serializers.DateField()
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=100)
    vote_count = serializers.FloatField()


class RestaurantUserVoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = RestaurantUserVote
        read_only_fields = ("vote_value",)
        fields = (
            "restaurant",
            "user",
            "vote_value",
        )

    def validate(self, data):
        daily_votes = RestaurantUserVote.objects.filter(
            created__date=timezone.localdate(), user=data["user"]
        ).count()
        max_votes = settings.MRV_MAX_DAILY_USER_VOTES
        if daily_votes >= max_votes:
            raise serializers.ValidationError(
                f"User exceeded daily voting limit of {max_votes} votes."
            )
        return data
