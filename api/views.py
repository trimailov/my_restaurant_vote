from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.filters import RestaurantUserVoteDateRangeFilter
from api.models import Restaurant, RestaurantUserVote
from api.serializers import (
    RestaurantSerializer,
    RestaurantUserVoteSerializer,
    RestaurantWinnerSerializer,
)


class RestaurantViewSet(viewsets.ModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        if self.action == "vote":
            return RestaurantUserVoteSerializer
        if self.action in ["winners", "todays_winner"]:
            return RestaurantWinnerSerializer
        else:
            return RestaurantSerializer

    @action(["post"], detail=True)
    def vote(self, request, pk=None):
        restaurant_pk = pk
        user_pk = self.request.user.pk
        vote_data = dict(
            user=user_pk,
            restaurant=restaurant_pk,
        )
        serializer = self.get_serializer_class()(data=vote_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(["get"], detail=False)
    def todays_winner(self, request):
        winner = RestaurantUserVote.objects.get_todays_winner()
        if winner is None:
            return Response(
                {"not_found": "no votes today"}, status=status.HTTP_404_NOT_FOUND
            )

        serializer = self.get_serializer_class()(data=winner)
        if serializer.is_valid():
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(["get"], detail=False)
    def winners(self, request):
        # XXX: Is this semantic DRF code? Or should we instantiate custom action, custom filters
        # somehow other way?
        filter = RestaurantUserVoteDateRangeFilter(
            data=request.query_params,
            queryset=RestaurantUserVote.objects.all(),
            request=request,
        )
        winners = filter.qs.get_winners()
        page = self.paginate_queryset(winners)
        serializer = self.get_serializer_class()(page, many=True)
        return self.get_paginated_response(serializer.data)
