FROM python:3.8.6-buster

COPY . /code
WORKDIR /code
RUN pip3 install --no-cache-dir -q -r requirements.txt

# this is necessary for heroku, as heroku does not allow root in container
RUN adduser --disabled-password --gecos "" my_vote_user
USER my_vote_user

ARG postgres_db
ARG postgres_user
ARG postgres_password
ARG postgres_host
ARG secret_key

ENV DJANGO_SETTINGS_MODULE=my_restaurant_vote.settings.heroku \
    POSTGRES_DB=$postgres_db \
    POSTGRES_USER=$postgres_user \
    POSTGRES_PASSWORD=$postgres_password \
    POSTGRES_HOST=$postgres_host \
    SECRET_KEY=$secret_key

RUN python manage.py collectstatic

CMD gunicorn --bind 0.0.0.0:$PORT my_restaurant_vote.wsgi
